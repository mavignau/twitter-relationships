#!/usr/bin/env python3

# Copyright 2019 María Andrea Vignau

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# For further info, check

__author__ = "María Andrea Vignau"

from twpy import TwpyClient
from twpy.serializers import to_list
import sqlite3

USERNAME = 'mavignau'


class Twitter_User:
    def __init__(self, username):
        self.tc = TwpyClient()
        self.username = username
        self.conn = sqlite3.connect(f"{username}.sqlite")
        self.get_field_names("relationships")
        # self.add_following()
        failed = 1
        while failed > 0:
            failed = self.update_users("relationships")

    def get_field_names(self, table):
        cursor = self.conn.execute(f'select * from {table}')
        self.field_names = [description[0] for description in cursor.description]

    def create_table(self):
        usr, c = self.seek_user(self.username, try_again=5)
        self.field_names = [k for k in dir(usr) if not k.startswith("_")]
        self.field_names.extend(["avatar", "fullname", "follower", "following"])
        self.create_table("relationships", self.field_names)

    def seek_user(self, username, try_again=2):
        for i in range(try_again):
            c = "#"
            try:
                usr = self.tc.get_user(username=username)
                if len(usr) > 0:
                    return usr[0], "_"
            except:
                c = "@"
        return None, c

    def create_table(self, table, field_list):
        stmt = f"CREATE TABLE {table} ({', '.join(field_list)})"
        self.see_stmt(stmt, values="")

    def update_table(self, table, key, object):
        fields = lambda a: ', '.join(list(a.keys()))
        filter = lambda a: ' and '.join([f'{k}=?' for k in a.keys()])
        updat = lambda a: ', '.join([f'{k}=?' for k in a.keys()])

        stmt = f"SELECT {fields(key)} FROM {table} WHERE {filter(key)}"
        cur = self.see_stmt(stmt, list(key.values()))
        row = cur.fetchone()
        if row and row[0]:
            stmt = f'UPDATE {table} SET {updat(object)} WHERE {filter(key)}'
            self.see_stmt(stmt, list(object.values()) + list(key.values()))
        else:
            object.update(key)
            stmt = f'INSERT INTO {table} ({fields(object)}) VALUES ({", ".join(["?"] * len(object))})'
            self.see_stmt(stmt, list(object.values()))
        self.conn.commit()

    def see_stmt(self, stament, values):
        # print(">> ", stament, " ?:", ','.join([str(v) for v in values]))
        cur = self.conn.cursor()
        cur.execute(stament, values)
        return cur

    def add_following(self):
        data = self.tc.get_friends(username=self.username, limit=1000, interval=1)
        for idx, obj in enumerate(data):
            keys, object = self.extract_values(obj, {'username': ""})
            object["following"] = "yes"
            self.update_table("relationships", keys, object)
        return idx

    def add_followers(self):
        data = self.tc.get_followers(username=self.username, limit=1000, interval=1)
        for idx, obj in enumerate(data):
            keys, object = self.extract_values(obj, {'username': ""})
            object["follower"] = "yes"
            self.update_table("relationships", keys, object)
        return idx

    def update_users(self, table, limit=10000):
        stmt = f"SELECT username FROM {table} WHERE user_id is null"
        cur = self.see_stmt(stmt, values="")
        failed = 0
        for idx, row in enumerate(cur.fetchall()):
            usr, c = self.seek_user(username=row[0])
            keys, object = self.extract_values(usr, {'username': ""})
            self.update_table(table, keys, object)
            print(c, end="")
            if idx > limit:
                break
            if idx % 90 == 0:
                print(' ', idx, failed)
            self.conn.commit()
            failed += 1 if c != "_" else 0
        self.conn.commit()
        return failed

    def extract_values(self, obj, keys):
        values = {}
        for k in dir(obj):
            if not k.startswith("_"):
                if k in self.field_names:
                    if k in keys:
                        keys[k] = getattr(obj, k)
                    else:
                        values[k] = getattr(obj, k)
        return keys, values


a = Twitter_User('mavignau')
