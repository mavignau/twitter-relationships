#!/usr/bin/env python3

# Copyright 2019 María Andrea Vignau

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# For further info, check

__author__ = "María Andrea Vignau"

from twpy import TwpyClient
from twpy.serializers import to_list
import csv

tc = TwpyClient()
tm = tc.get_timeline(username="Anonmehicieron", limit=250, interval=1)

with open("sample.csv", "w", encoding="utf8") as fh:
    writer = csv.DictWriter(fh, fieldnames=[f for f in dir(tm[0]) if not f.startswith("_")])
    writer.writeheader()
    for t in tm:
        data = {}
        for f in writer.fieldnames:
            data[f] = getattr(t, f, None)
        writer.writerow(data)
